import de.learnlib.acex.AcexAnalyzer;
import de.learnlib.algorithm.kv.dfa.KearnsVaziraniDFA;
import de.learnlib.algorithm.kv.dfa.KearnsVaziraniDFABuilder;
import de.learnlib.algorithm.lstar.dfa.ClassicLStarDFABuilder;
import de.learnlib.algorithm.observationpack.dfa.OPLearnerDFA;
import de.learnlib.algorithm.observationpack.dfa.OPLearnerDFABuilder;
import de.learnlib.algorithm.rivestschapire.RivestSchapireDFA;
import de.learnlib.algorithm.ttt.base.AbstractTTTLearner;
import de.learnlib.algorithm.ttt.dfa.TTTLearnerDFA;
import de.learnlib.algorithm.ttt.dfa.TTTLearnerDFABuilder;
import de.learnlib.filter.statistic.oracle.DFACounterOracle;
import de.learnlib.oracle.EquivalenceOracle;
import de.learnlib.oracle.MembershipOracle;
import de.learnlib.oracle.equivalence.DFASimulatorEQOracle;
import de.learnlib.oracle.equivalence.WMethodEQOracle;
import de.learnlib.oracle.membership.DFASimulatorOracle;
import de.learnlib.query.DefaultQuery;
import net.automatalib.alphabet.Alphabet;
import net.automatalib.alphabet.Alphabets;
import net.automatalib.automaton.fsa.DFA;
import de.learnlib.algorithm.lstar.dfa.ClassicLStarDFA;
import de.learnlib.algorithm.rivestschapire.RivestSchapireDFABuilder;
import net.automatalib.word.Word;
import org.knowm.xchart.QuickChart;
import org.knowm.xchart.SwingWrapper;
import org.knowm.xchart.XYChart;
import org.knowm.xchart.XYSeries;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class ExperimentsMinimalCounterExampleLength {

    public static void main(String[] args) {
        // Define configurations
        int[] yValues = { 5 };
        int[] zValues = { 100 };
        double[] xValues = { 0.1 };
        int[] minimal_counterexample_length ={10,20,30,40,50,60,70,80};


        // Declare arrays to store the average membership queries and average equivalence queries
        double[] avgMemQueriesLstar = new double[minimal_counterexample_length.length];
        double[] avgMemQueriesRS = new double[minimal_counterexample_length.length];
        double[] avgMemQueriesKV = new double[minimal_counterexample_length.length];
        double[] avgMemQueriesOP = new double[minimal_counterexample_length.length];
        double[] avgMemQueriesTTT = new double[minimal_counterexample_length.length];

        double[] avgEqQueriesLstar = new double[minimal_counterexample_length.length];
        double[] avgEqQueriesRS = new double[minimal_counterexample_length.length];
        double[] avgEqQueriesKV = new double[minimal_counterexample_length.length];
        double[] avgEqQueriesOP = new double[minimal_counterexample_length.length];
        double[] avgEqQueriesTTT = new double[minimal_counterexample_length.length];





        // Iterate over each configuration: y denotes the size of the alphabet of the target DFA, z denotes the size of the target DFA, x denotes the acceptance ratio of the target DFA
        for(int y:yValues) {
            for (int z : zValues) {
                int k=0;
                for (double x : xValues) {
                    // Create the alphabet
                    Alphabet<Integer> alphabet = Alphabets.integers(0, y - 1);



                    for(int v : minimal_counterexample_length) {
                        System.out.println("Minimal counterexample length: " + v);

                        int memCounterLstar = 0;
                        int memCounterRS = 0;
                        int memCounterKV = 0;
                        int memCounterOP = 0;
                        int memCounterTTT = 0;

                        int eqCounterLstar = 0;
                        int eqCounterRS = 0;
                        int eqCounterKV = 0;
                        int eqCounterOP = 0;
                        int eqCounterTTT = 0;

                        // Generate 100 random DFAs for each configuration
                        for (int i = 0; i < 100; i++) {
                            //System.out.println("Generating random DFA " + i + " for configuration (x,y,z):" + x + "," + y + "," + z);
                            System.out.println("Generation random DFA" + i + "with minimal counterexample length: " + v + "and 100 states, acceptence ration 0.1 and alphabet size 5");
                            Random rand = new Random();
                            ExampleRandomDFA example = new ExampleRandomDFA(rand, y, z, x, alphabet);
                            DFA<?, Integer> dfa = example.getReferenceAutomaton();

                            // Initialize the Query Counters and the Oracles - we used the white-box oracles
                            EquivalenceOracle<DFA<?, Integer>, Integer, Boolean> eqOracleLstar = new DFASimulatorEQOracle<>(dfa);
                            EQCounterOracle<DFA<?, Integer>, Integer, Boolean> eqCounterOracleLstar = new EQCounterOracle<>(eqOracleLstar);
                            MembershipOracle.DFAMembershipOracle<Integer> memOracleLstar = new DFASimulatorOracle<>(dfa);
                            DFACounterOracle<Integer> memCounterOracleLstar = new DFACounterOracle<>(memOracleLstar);

                            EquivalenceOracle<DFA<?, Integer>, Integer, Boolean> eqOracleRS = new DFASimulatorEQOracle<>(dfa);
                            EQCounterOracle<DFA<?, Integer>, Integer, Boolean> eqCounterOracleRS = new EQCounterOracle<>(eqOracleRS);
                            MembershipOracle.DFAMembershipOracle<Integer> memOracleRS = new DFASimulatorOracle<>(dfa);
                            DFACounterOracle<Integer> memCounterOracleRS = new DFACounterOracle<>(memOracleRS);

                            EquivalenceOracle<DFA<?, Integer>, Integer, Boolean> eqOracleKV = new DFASimulatorEQOracle<>(dfa);
                            EQCounterOracle<DFA<?, Integer>, Integer, Boolean> eqCounterOracleKV = new EQCounterOracle<>(eqOracleKV);
                            MembershipOracle.DFAMembershipOracle<Integer> memOracleKV = new DFASimulatorOracle<>(dfa);
                            DFACounterOracle<Integer> memCounterOracleKV = new DFACounterOracle<>(memOracleKV);

                            EquivalenceOracle<DFA<?, Integer>, Integer, Boolean> eqOracleOP = new DFASimulatorEQOracle<>(dfa);
                            EQCounterOracle<DFA<?, Integer>, Integer, Boolean> eqCounterOracleOP = new EQCounterOracle<>(eqOracleOP);
                            MembershipOracle.DFAMembershipOracle<Integer> memOracleOP = new DFASimulatorOracle<>(dfa);
                            DFACounterOracle<Integer> memCounterOracleOP = new DFACounterOracle<>(memOracleOP);

                            EquivalenceOracle<DFA<?, Integer>, Integer, Boolean> eqOracleTTT = new DFASimulatorEQOracle<>(dfa);
                            EQCounterOracle<DFA<?, Integer>, Integer, Boolean> eqCounterOracleTTT = new EQCounterOracle<>(eqOracleTTT);
                            MembershipOracle.DFAMembershipOracle<Integer> memOracleTTT = new DFASimulatorOracle<>(dfa);
                            DFACounterOracle<Integer> memCounterOracleTTT = new DFACounterOracle<>(memOracleTTT);


                            // Learn the DFA using each learning algorithm
                            learnWithClassicLStarDFA(dfa, alphabet, eqCounterOracleLstar, memCounterOracleLstar,v);
                            learnWithRivestShapireDFA(dfa, alphabet, eqCounterOracleRS, memCounterOracleRS,v);
                            learnWithKearnsVauiraniDFA(dfa, alphabet, eqCounterOracleKV, memCounterOracleKV,v);
                            learnWithOPLearnerDFA(dfa, alphabet, eqCounterOracleOP, memCounterOracleOP,v);
                            learnWithTTTLearnerDFA(dfa, alphabet, eqCounterOracleTTT, memCounterOracleTTT,v);

                            // Update the counters
                            memCounterLstar += memCounterOracleLstar.getQueryCounter().getCount();
                            memCounterRS += memCounterOracleRS.getQueryCounter().getCount();
                            memCounterKV += memCounterOracleKV.getQueryCounter().getCount();
                            memCounterOP += memCounterOracleOP.getQueryCounter().getCount();
                            memCounterTTT += memCounterOracleTTT.getQueryCounter().getCount();

                            eqCounterLstar += eqCounterOracleLstar.getCounter();
                            eqCounterRS += eqCounterOracleRS.getCounter();
                            eqCounterKV += eqCounterOracleKV.getCounter();
                            eqCounterOP += eqCounterOracleOP.getCounter();
                            eqCounterTTT += eqCounterOracleTTT.getCounter();
                        }
                        //Calculate average for configuration (x,y,z) for the counters


                        System.out.println("For configuration (x,y,z):" + x + "," + y + "," + z + " :");
                        System.out.println("Average memCounterLstar: " + memCounterLstar / 100);
                        System.out.println("Average memCounterRS: " + memCounterRS / 100);
                        System.out.println("Average memCounterKV: " + memCounterKV / 100);
                        System.out.println("Average memCounterOP: " + memCounterOP / 100);
                        System.out.println("Average memCounterTTT: " + memCounterTTT / 100);

                        System.out.println("Average eqCounterLstar: " + eqCounterLstar / 100);
                        System.out.println("Average eqCounterRS: " + eqCounterRS / 100);
                        System.out.println("Average eqCounterKV: " + eqCounterKV / 100);
                        System.out.println("Average eqCounterOP: " + eqCounterOP / 100);
                        System.out.println("Average eqCounterTTT: " + eqCounterTTT / 100);
                        // Calculate the averages and store them in the arrays
                        avgMemQueriesLstar[k] = memCounterLstar / 100.0;
                        avgMemQueriesRS[k] = memCounterRS / 100.0;
                        avgMemQueriesKV[k] = memCounterKV / 100.0;
                        avgMemQueriesOP[k] = memCounterOP / 100.0;
                        avgMemQueriesTTT[k] = memCounterTTT / 100.0;

                        avgEqQueriesLstar[k] = eqCounterLstar / 100.0;
                        avgEqQueriesRS[k] = eqCounterRS / 100.0;
                        avgEqQueriesKV[k] = eqCounterKV / 100.0;
                        avgEqQueriesOP[k] = eqCounterOP / 100.0;
                        avgEqQueriesTTT[k] = eqCounterTTT / 100.0;
                        k++;
                    }



                }
            }
        }
        String[] seriesNames = {"LStar","RS","KV", "OP", "TTT"};
        double[] minimal_counterexample_length_double = Arrays.stream(minimal_counterexample_length)
                .asDoubleStream()
                .toArray();

// Create the first XY chart (membership queries)
        XYChart chart1 = QuickChart.getChart("Average Membership Queries", "Minimal Counterexamplelength", "Queries", seriesNames, minimal_counterexample_length_double, new double[][]{avgMemQueriesLstar,avgMemQueriesRS, avgMemQueriesKV, avgMemQueriesOP, avgMemQueriesTTT});
        new SwingWrapper(chart1).displayChart();

// Create the second XY chart (equivalence queries)
        XYChart chart2 = QuickChart.getChart("Average Equivalence Queries", "Minimal Counterexamplelength", "Queries", seriesNames, minimal_counterexample_length_double, new double[][]{avgEqQueriesLstar, avgEqQueriesRS,avgEqQueriesKV, avgEqQueriesOP, avgEqQueriesTTT});
        new SwingWrapper(chart2).displayChart();

        printXYChartPairs(chart1);
        printXYChartPairs(chart2);
    }

    // Define the learning methods
    public static void learnWithClassicLStarDFA(DFA<?, Integer> dfa, Alphabet<Integer> alphabet, EquivalenceOracle<DFA<?, Integer>, Integer, Boolean> eqOracleLstar, MembershipOracle.DFAMembershipOracle<Integer> memOracleLstar, int lb) {
        // Create a membership oracle from the DFA


        // Create an instance of the ClassicLStar algorithm
        ClassicLStarDFA<Integer> learner =
                new ClassicLStarDFABuilder<Integer>().withAlphabet(alphabet) // input alphabet
                        .withOracle(memOracleLstar) // membership oracle
                        .create();

        // Start the learning process
        learner.startLearning();

        // Create an equivalence oracle


        int start = lb;
        // Perform equivalence queries until no counterexample is found
        DefaultQuery<Integer, Boolean> counterexample;
        while ((counterexample = eqOracleLstar.findCounterExample(learner.getHypothesisModel(), alphabet)) != null) {
            // Refine the model using the counterexample
            lb=start;
            Boolean targetOutput;
            Boolean hypothesisOutput;
            Word<Integer> word;
            int k = 0;
            do {
                if(k==1000){lb++;k=0;} // Increase the length of the counterexample if no counterexample is found fast enough
                // Create a new random word of length 20
                Random rand = new Random();
                List<Integer> wordList = new ArrayList<>();
                for (int i = 0; i < lb; i++) {
                    int randomIndex = rand.nextInt(alphabet.size());
                    wordList.add(alphabet.getSymbol(randomIndex));
                }
                // Convert the list to a Word
                word = Word.fromList(wordList);

                // Get the output of the target DFA for the word
                targetOutput = dfa.computeOutput(word);
                hypothesisOutput = learner.getHypothesisModel().computeOutput(word);
                k++;
            } while (targetOutput == hypothesisOutput);
            DefaultQuery<Integer, Boolean> counterexample2;

            counterexample2 = new DefaultQuery<>(word, targetOutput);
            if(word.length()>=counterexample.getInput().length()&& counterexample.getInput().length()>= start){counterexample2=counterexample;}

            learner.refineHypothesis(counterexample2);
        }


        // The learned model can be obtained by calling learner.getModel()

    }

    public static void learnWithRivestShapireDFA(DFA<?, Integer> dfa, Alphabet<Integer> alphabet, EquivalenceOracle<DFA<?, Integer>, Integer, Boolean> eqOracleRS, MembershipOracle.DFAMembershipOracle<Integer> memOracleRS, int lb) {
        // Implement learning with RivestShapireDFA
        RivestSchapireDFABuilder<Integer> rsBuilder = new RivestSchapireDFABuilder<>();
        rsBuilder.withAlphabet(alphabet); // input alphabet
        rsBuilder.withOracle(memOracleRS); // membership oracle
        RivestSchapireDFA<Integer> learner = rsBuilder.create();
        learner.startLearning();
        DefaultQuery<Integer, Boolean> counterexample;
        int start = lb;

        while ((counterexample = eqOracleRS.findCounterExample(learner.getHypothesisModel(), alphabet)) != null) {
            // Refine the model using the counterexample
            lb=start;
            Boolean targetOutput;
            Boolean hypothesisOutput;
            Word<Integer> word;

            int k = 0;
            do {
                if(k==1000){lb++;k=0;} // Increase the length of the counterexample if no counterexample is found fast enough
                Random rand = new Random();
                List<Integer> wordList = new ArrayList<>();
                for (int i = 0; i < lb; i++) {
                    int randomIndex = rand.nextInt(alphabet.size());
                    wordList.add(alphabet.getSymbol(randomIndex));
                }
                // Convert the list to a Word
                word = Word.fromList(wordList);

                // Get the output of the target DFA for the word
                targetOutput = dfa.computeOutput(word);
                hypothesisOutput = learner.getHypothesisModel().computeOutput(word);
                k++;
            } while (targetOutput == hypothesisOutput);
            DefaultQuery<Integer, Boolean> counterexample2;

            counterexample2 = new DefaultQuery<>(word, targetOutput);
            if(word.length()>=counterexample.getInput().length()&& counterexample.getInput().length()>= start){counterexample2=counterexample;}

            learner.refineHypothesis(counterexample2);
        }

    }

    public static void learnWithKearnsVauiraniDFA(DFA<?, Integer> dfa, Alphabet<Integer> alphabet, EquivalenceOracle<DFA<?, Integer>, Integer, Boolean> eqOracleKV, MembershipOracle.DFAMembershipOracle<Integer> memOracleKV, int lb) {
        KearnsVaziraniDFABuilder<Integer> kol= new KearnsVaziraniDFABuilder<Integer>();
        kol.withAlphabet(alphabet);
        kol.withOracle(memOracleKV);
        kol.withRepeatedCounterexampleEvaluation(true);
        KearnsVaziraniDFA<Integer> learner = kol.create();
        learner.startLearning();
        DefaultQuery<Integer, Boolean> counterexample;
        int start = lb;
        while ((counterexample = eqOracleKV.findCounterExample(learner.getHypothesisModel(), alphabet)) != null) {
            lb=start;
            Boolean targetOutput;
            Boolean hypothesisOutput;
            Word<Integer> word;

            int k = 0;
            do {
                if(k==1000){lb++;k=0;} // Increase the length of the counterexample if no counterexample is found fast enough
                Random rand = new Random();
                List<Integer> wordList = new ArrayList<>();
                for (int i = 0; i < lb; i++) {
                    int randomIndex = rand.nextInt(alphabet.size());
                    wordList.add(alphabet.getSymbol(randomIndex));
                }
                // Convert the list to a Word
                word = Word.fromList(wordList);

                // Get the output of the target DFA for the word
                targetOutput = dfa.computeOutput(word);
                hypothesisOutput = learner.getHypothesisModel().computeOutput(word);
                k++;
            } while (targetOutput == hypothesisOutput);
            DefaultQuery<Integer, Boolean> counterexample2;

            counterexample2 = new DefaultQuery<>(word, targetOutput);
            if(word.length()>=counterexample.getInput().length()&& counterexample.getInput().length()>= start){counterexample2=counterexample;}

            learner.refineHypothesis(counterexample2);
        }

    }

    public static void learnWithOPLearnerDFA(DFA<?, Integer> dfa, Alphabet<Integer> alphabet, EquivalenceOracle<DFA<?, Integer>, Integer, Boolean> eqOracleOP, MembershipOracle.DFAMembershipOracle<Integer> memOracleOP, int lb) {

        OPLearnerDFABuilder<Integer> opBuilder = new OPLearnerDFABuilder<>();
        opBuilder.withAlphabet(alphabet);
        opBuilder.withOracle(memOracleOP);
        opBuilder.setRepeatedCounterexampleEvaluation(true);
        OPLearnerDFA<Integer> learner = opBuilder.create();


        // Perform equivalence queries until no counterexample is found
        DefaultQuery<Integer, Boolean> counterexample;
        learner.startLearning();
        int start = lb;
        while ((counterexample = eqOracleOP.findCounterExample(learner.getHypothesisModel(), alphabet)) != null) {
            // Refine the model using the counterexample
            Boolean targetOutput;
            Boolean hypothesisOutput;
            Word<Integer> word;
            lb=start;
            int k = 0;
            do {
                if(k==1000){lb++;k=0;} // Increase the length of the counterexample if no counterexample is found fast enough
                Random rand = new Random();
                List<Integer> wordList = new ArrayList<>();
                for (int i = 0; i < lb; i++) {
                    int randomIndex = rand.nextInt(alphabet.size());
                    wordList.add(alphabet.getSymbol(randomIndex));
                }
                // Convert the list to a Word
                word = Word.fromList(wordList);

                // Get the output of the target DFA for the word
                targetOutput = dfa.computeOutput(word);
                hypothesisOutput = learner.getHypothesisModel().computeOutput(word);
                k++;
            } while (targetOutput == hypothesisOutput);
            DefaultQuery<Integer, Boolean> counterexample2;

            counterexample2 = new DefaultQuery<>(word, targetOutput);
            if(word.length()>=counterexample.getInput().length()&& counterexample.getInput().length()>= start){counterexample2=counterexample;}
            learner.refineHypothesis(counterexample2);

        }
    }

    public static void learnWithTTTLearnerDFA(DFA<?, Integer> dfa, Alphabet<Integer> alphabet, EquivalenceOracle<DFA<?, Integer>, Integer, Boolean> oracle, MembershipOracle.DFAMembershipOracle<Integer> mem, int lb) {
        TTTLearnerDFA<Integer> learner =
                new TTTLearnerDFABuilder<Integer>().withAlphabet(alphabet)
                        .withOracle(mem)
                        .create();

        learner.startLearning();


        // Perform equivalence queries until no counterexample is found
        DefaultQuery counterexample;
        int start = lb;
        while ((counterexample = oracle.findCounterExample(learner.getHypothesisModel(), alphabet)) != null) {
            // Refine the model using the counterexample
            Boolean targetOutput;
            lb=start;
            Boolean hypothesisOutput;
            Word<Integer> word;
            int k = 0;
            do {
                if(k==1000){lb++;k=0;} // Increase the length of the counterexample if no counterexample is found fast enough
                Random rand = new Random();
                List<Integer> wordList = new ArrayList<>();
                for (int i = 0; i < 100; i++) {
                    int randomIndex = rand.nextInt(alphabet.size());
                    wordList.add(alphabet.getSymbol(randomIndex));
                }
                // Convert the list to a Word
                word = Word.fromList(wordList);

                // Get the output of the target DFA for the word
                targetOutput = dfa.computeOutput(word);
                hypothesisOutput = learner.getHypothesisModel().computeOutput(word);
                k++;
            } while (targetOutput == hypothesisOutput);
            DefaultQuery<Integer, Boolean> counterexample2;

            counterexample2 = new DefaultQuery<>(word, targetOutput);
            if(word.length()>=counterexample.getInput().length()&& counterexample.getInput().length()>= start){counterexample2=counterexample;}
            learner.refineHypothesis(counterexample2);

        }
    }

    public static void printXYChartPairs(XYChart chart) {
        // Get all series in the chart
        System.out.println("Chart: " + chart.getTitle());
        List<XYSeries> seriesList = chart.getSeriesMap().values().stream().collect(Collectors.toList());

        // Iterate over each series
        for (XYSeries series : seriesList) {
            System.out.println("Series: " + series.getName());

            // Get x and y data
            double[] xData = series.getXData();
            double[] yData = series.getYData();

            // Ensure x and y data are of the same size
            if (xData.length != yData.length) {
                System.out.println("Mismatch in data size for series: " + series.getName());
                continue;
            }

            // Print (x, y) pairs
            for (int i = 0; i < xData.length; i++) {
                System.out.println("(" + xData[i] + ", " + yData[i] + ")");
            }
        }
    }


}
