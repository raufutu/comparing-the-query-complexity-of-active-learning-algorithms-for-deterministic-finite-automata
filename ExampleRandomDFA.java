import net.automatalib.alphabet.Alphabet;
import net.automatalib.automaton.fsa.CompactDFA;
import net.automatalib.util.automaton.minimizer.hopcroft.HopcroftMinimization;
import net.automatalib.util.automaton.random.RandomAutomata;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class ExampleRandomDFA extends DefaultLearningExample.DefaultDFALearningExample<Integer> {

    public ExampleRandomDFA(Random rand, int numInputs, int size, double acceptanceRatio, Alphabet<Integer> alphabet) {
        super(alphabet, randomDFAWithAcceptanceRatio(rand, size, alphabet, acceptanceRatio));
    }

    private static CompactDFA<Integer> randomDFAWithAcceptanceRatio(Random rand, int size, Alphabet<Integer> alphabet, double acceptanceRatio) {
        CompactDFA<Integer> newDFA = null;
        CompactDFA<Integer> dfa = null;

        do {
            boolean flag = true;
            int counter = 0;
            while(flag) {
                dfa = RandomAutomata.randomDFA(rand, size, alphabet);
                // Iterate over all states of dfa and make them rejecting
                if(dfa.size()==(size)) {
                    flag = false;
                }
            }
            for (Integer state : dfa.getStates()) {
                dfa.setAccepting(state, false);
            }
            // there should be size*acceptanceRatio accepting states, the accepting states should be randomly picked from the states but no double assingnments
            ArrayList<Integer> stateIndices = new ArrayList<>(dfa.getStates());
            Collections.shuffle(stateIndices, rand);
            int numAcceptingStates = (int) (size * acceptanceRatio);
            if(numAcceptingStates == 0) {
                numAcceptingStates = 1;
            }
            for (int i = 0; i < numAcceptingStates; i++) {
                dfa.setAccepting(stateIndices.get(i), true);
            }

            newDFA = HopcroftMinimization.minimizeDFA(dfa, alphabet);
            if(newDFA.size() != size){
                rand = new Random(333+counter);
            }
        } while (newDFA.size() != size);

        return newDFA;
    }
}